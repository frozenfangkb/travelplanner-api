import express from 'express';
import https, {ServerOptions} from 'https';
import * as dotenv from 'dotenv';
//import userRouter from './routes/UserRoutes';
import EmailNotifyRouter from './routes/EmailNotify';
import bodyParser from "body-parser";
import { disconnectFromDb } from './database/database';
import cors from "cors";
import * as fs from "fs";

dotenv.config();

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors({ origin: "*" }));

//app.use('/api/users', userRouter);
app.use('/api/notify', EmailNotifyRouter);

const options: ServerOptions = {
    key: fs.readFileSync(__dirname + '/certs/travel-planner-private.key'),
    cert: fs.readFileSync(__dirname + '/certs/travel-planner-certificate.cer'),
    requestCert: true,
    rejectUnauthorized: false,
};

switch (process.env.ENVIRONMENT) {
    case 'local':
        app.listen(port, () => {
            console.log(`Travelplanner API working on port ${port}. Using HTTP method`);
        });
        break;
    default:
        const server: https.Server = https.createServer(options, app);

        server.listen(port, () => {
            console.log(`Travelplanner API working on port ${port}. Secured by SSL.`);
        });
        break;
}

app.on('exit', async () => await disconnectFromDb());
