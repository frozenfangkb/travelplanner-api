import { connection } from "mongoose";
import { createTransport } from "nodemailer";
import { connectToDb } from "../database/database";
import { INotifiedUser, NotifiedUser } from "../models/NotifiedUser";
import { validateEmail } from "../utils/EmailValidator";
import * as dotenv from 'dotenv';
import {Request, Response, NextFunction} from "express/ts4.0";

dotenv.config();

const transporter = createTransport({
    host: process.env.SMTP_HOST,
    port: parseInt(process.env.SMTP_PORT as string),
    auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD,
    },
    requireTLS: true
});


export const RegisterEmail = async (req: Request,res: Response,next: NextFunction) => {
    try {
        if (connection.readyState !== 1) {
            await connectToDb();
        }
        
        const newUser: INotifiedUser = req.body;
        if (!newUser || !newUser.email || !validateEmail(newUser.email)) {
            res.status(400).json({ message: 'No email present or invalid email' });
            return;
        }
        if ((await NotifiedUser.find({ email: newUser.email })).length > 0) {
            res.json({ message: 'User already registered' });
        } else {
            if ((await NotifiedUser.find({ from_ip: req.ip })).length > 3) {
                res.status(403).send();
                return;
            }
            const notifiedUser = new NotifiedUser(newUser);
            notifiedUser.from_ip = req.ip;
            await notifiedUser.save();
            await transporter.sendMail({
                from: 'info@travel-planner.es',
                to: newUser.email,
                subject: 'Registro en TravelPlanner completado correctamente',
                html: '<h1>¡Hola!</h1><br /><p>Hemos registrado esta dirección para enviar las novedades de TravelPlanner en cuanto esté disponible.</p><p>¡Muchas gracias!</p><p>El equipo de TravelPlanner</p>'
            });
            res.status(201).json({ message: 'User registered successfully' });
        }
    } catch (err) {
        res.status(500).json({ message: 'Internal server error' });
    }
};
