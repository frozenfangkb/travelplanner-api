import { connect, ConnectOptions, disconnect } from 'mongoose';

export async function connectToDb(): Promise<void> {
    connect(process.env.MONGODB_CONNECTION_STRING as string,{
        useNewUrlParser: true,
        useUnifiedTopology: true
    } as ConnectOptions).then(() => {
        if (process.env.ENVIRONMENT === 'local') {
            console.info('Connected successfully to database');
        }
    }).catch((error: Error) => console.log(error.message));
}

export async function disconnectFromDb(): Promise<void> {
    await disconnect().then(() => console.info('Disconnected from db'));
}
