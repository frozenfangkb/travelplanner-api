import { Schema, model } from 'mongoose';

interface ICountry {
    name: string;
    continent_name: string;
}

const schema = new Schema<ICountry>({
    name: { type: String, required: true },
    continent_name: { type: String, required: true },
});

export const Country = model<ICountry>('User', schema);