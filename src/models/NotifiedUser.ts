import { Schema, model } from 'mongoose';

export interface INotifiedUser {
    email: string;
    from_ip: string;
    created_at: Date;
}

const schema = new Schema<INotifiedUser>({
    email: { type: String, required: true },
    from_ip: { type: String, required: true },
}, { timestamps: { createdAt: 'created_at' } });

export const NotifiedUser = model<INotifiedUser>('NotifiedUser', schema);
