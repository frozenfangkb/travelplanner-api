import { Schema, model } from 'mongoose';

interface ITravel {
    name: string;
    owner_id: Schema.Types.ObjectId;
    start_date: Date;
    end_date: Date;
    created_at: Date;
    updated_at: Date;
}

const schema = new Schema<ITravel>({
    name: { type: String, required: true },
    owner_id: { type: Schema.Types.ObjectId, ref: 'Country' },
    start_date: { type: Date },
    end_date: { type: Date },
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

export const Travel = model<ITravel>('User', schema);