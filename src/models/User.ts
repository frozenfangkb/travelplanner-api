import { Schema, model } from 'mongoose';

export interface IUser {
    first_name: string;
    last_name: string;
    email: string;
    avatar?: string;
    country_code: Schema.Types.ObjectId;
    created_at: Date;
    updated_at: Date;
}

const schema = new Schema<IUser>({
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    email: { type: String, required: true },
    country_code: { type: Schema.Types.ObjectId, ref: 'Country' },
    avatar: String,
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

export const User = model<IUser>('User', schema);