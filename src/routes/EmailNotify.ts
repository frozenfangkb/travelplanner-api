import {Router} from "express";
import { RegisterEmail } from "../controllers/EmailController";

const router = Router();

router.post('/register', RegisterEmail);

export default router;
